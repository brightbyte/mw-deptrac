<?php
namespace Wikimedia\Phanalyst\Visitor;

use PhpParser\Comment\Doc;
use PhpParser\Node;
use PhpParser\Node\AttributeGroup;
use PhpParser\Node\Expr\Closure;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Function_;
use PhpParser\Node\Stmt\Interface_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Property;
use PhpParser\Node\Stmt\Trait_;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\Node\Stmt\TraitUseAdaptation;
use PhpParser\NodeVisitorAbstract;
use Wikimedia\Phanalyst\Analysis\AnalysisException;
use Wikimedia\Phanalyst\Model\ClassLikeScope;
use Wikimedia\Phanalyst\Model\ClassLikeType;
use Wikimedia\Phanalyst\Model\FunctionLikeScope;
use Wikimedia\Phanalyst\Model\MethodLikeScope;
use Wikimedia\Phanalyst\Model\NamespaceScope;
use Wikimedia\Phanalyst\Analysis\CodeBase;
use Wikimedia\Phanalyst\Model\Tag;

class DeclarationCollector extends NodeVisitorAbstract {
	// TODO: FileLocation
	private string $file;

	private CodeBase $codeBase;
	private ?NamespaceScope $namespace = null;
	private ?ClassLikeScope $class = null;
	private ?FunctionLikeScope $function = null;

	/**
	 * @param string $file
	 * @param CodeBase $codeBase
	 */
	public function __construct( string $file, CodeBase $codeBase ) {
		$this->file = $file;
		$this->codeBase = $codeBase;
	}

	private function augmentAttributes( Node $node ) {
		$node->setAttribute( 'filePath', $this->file );
	}

	private function precondition( bool $ok, string $msg, Node $node ) {
		if ( !$ok ) {
			throw new AnalysisException( $msg, $node );
		}
	}

	private function getCurrentNamespace(): NamespaceScope {
		if ( !$this->namespace ) {
			$this->namespace = $this->codeBase->getRootNamespace();
		}

		return $this->namespace;
	}

	private function getCurrentClass( Node $context ): ClassLikeScope {
		$this->precondition( $this->class !== null, 'No current class!', $context );
		return $this->class;
	}

	public function enterNode(Node $node) {
		$this->augmentAttributes( $node );

		if ( $node instanceof Function_ ) {
			if ( $this->function ) {
				// TODO: maintain a stack of scopes
				throw new AnalysisException( 'Nested functions are not yet supported', $node );
			}
			$ns = $this->getCurrentNamespace();
			$this->function = $ns->declareFunction( $node->name );
		} elseif ( $node instanceof Closure ) {
			// TODO: support "use" params
			throw new AnalysisException( 'Closures are not yet supported', $node );
		} elseif ( $node instanceof ClassMethod ) {
			$class = $this->getCurrentClass( $node );
			$this->function = $class->declareFunction( $node->name );
		} elseif ( $node instanceof Interface_ ) {
			$this->class = $this->codeBase->declareClass(
				$this->getCurrentNamespace(),
				$node->name,
				ClassLikeType::INTERFACE_TYPE
			);
		} elseif ( $node instanceof Class_ ) {
			$this->class = $this->codeBase->declareClass(
				$this->getCurrentNamespace(),
				$node->name,
				ClassLikeType::CLASS_TYPE
			);
		} elseif ( $node instanceof Trait_ ) {
			$this->class = $this->codeBase->declareClass(
				$this->getCurrentNamespace(),
				$node->name,
				ClassLikeType::TRAIT_TYPE
			);
		} elseif ( $node instanceof Namespace_ ) {
			$this->namespace = $this->codeBase->getNamespace( $node->name );
		}
	}

	public function leaveNode(Node $node) {
		if ( $node instanceof Function_ || $node instanceof ClassMethod ) {

			if ( $node->getDocComment() ) {
				$tags = $this->extractTagsFromComment( $node->getDocComment() );
				$this->codeBase->addFunctionTags( $this->function, $tags );
			}

			if ( $node->getAttrGroups() ) {
				$tags = $this->extractTagsFromAttributes( $node->getAttrGroups() );
				$this->codeBase->addFunctionTags( $this->function, $tags );
			}

			$this->codeBase->setFunctionModifiers( $this->function, $node );

			// FIXME: add param types from doc block
			$this->codeBase->setFunctionParameters( $this->function, $node->getParams() );

			if ( $node->returnType ) {
				$this->codeBase->setFunctionReturnType(
					$this->function,
					$node->returnType
				);
			}

			$this->function = null;
		} elseif ( $node instanceof Interface_ ) {
			if ( $node->extends ) {
				$this->codeBase->addClassExtends(
					$this->getCurrentClass( $node ),
					$node->extends
				);
			}
		} elseif ( $node instanceof Class_ ) {
			if ( $node->extends ) {
				$this->codeBase->addClassExtends(
					$this->getCurrentClass( $node ),
					[ $node->extends ]
				);
			}
			if ( $node->implements ) {
				$this->codeBase->addClassImplements(
					$this->getCurrentClass( $node ),
					$node->implements
				);
			}
			$this->class = null;
		} elseif ( $node instanceof Trait_ ) {
			$this->class = null;
		} elseif ( $node instanceof TraitUse ) {
			$this->codeBase->addClassTraits(
				$this->getCurrentClass( $node ),
				$node->traits
			);
		} elseif ( $node instanceof Property ) {
			// TODO!
		} elseif ( $node instanceof TraitUseAdaptation ) {
			// TODO!
		} elseif ( $node instanceof Namespace_ ) {
			$this->namespace = null;
		}

		// print " -- " . get_class( $node ) . "\n";
	}

	/**
	 * @param Doc|null $docComment
	 *
	 * @return Tag[]
	 */
	private function extractTagsFromComment( ?Doc $docComment ): array {
		// TODO: parse doc block and extract doxygen tags
		return [];
	}

	/**
	 * @param AttributeGroup[] $attrGroups
	 *
	 * @return Tag[]
	 */
	private function extractTagsFromAttributes( array $attrGroups ): array {
		// TODO: process attr groups
		return [];
	}

}