<?php
namespace Wikimedia\Phanalyst\Visitor;

use PhpParser\Node\Stmt\Trait_;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor;
use Wikimedia\Phanalyst\Analysis\AnalysisException;
use Wikimedia\Phanalyst\Model\Call;
use Wikimedia\Phanalyst\Model\ClassLikeScope;
use Wikimedia\Phanalyst\Model\ClassLikeType;
use Wikimedia\Phanalyst\Model\InstructionScope;
use Wikimedia\Phanalyst\Model\FileScope;
use Wikimedia\Phanalyst\Model\FunctionLikeScope;
use Wikimedia\Phanalyst\Model\FunctionLikeRef;
use Wikimedia\Phanalyst\Model\MethodLikeRef;
use Wikimedia\Phanalyst\Model\MethodLikeScope;
use Wikimedia\Phanalyst\Model\NamespaceScope;
use PhpParser\Node;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\NullsafeMethodCall;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Function_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeVisitorAbstract;
use Wikimedia\Phanalyst\Analysis\CodeBase;

class LenientCollector implements NodeVisitor {

	private NodeVisitor $visitor;

	public function __construct( NodeVisitor $visitor ) {
		$this->visitor = $visitor;
	}

	public function beforeTraverse( array $nodes ) {
		try {
			$this->visitor->beforeTraverse( $nodes );
		} catch ( AnalysisException $e ) {
			$this->logError( $e );
		}
	}

	public function enterNode( Node $node ) {
		try {
			$this->visitor->enterNode( $node );
		} catch ( AnalysisException $e ) {
			$this->logError( $e );
			return NodeTraverser::DONT_TRAVERSE_CHILDREN;
		}
	}

	public function leaveNode( Node $node ) {
		try {
			$this->visitor->leaveNode( $node );
		} catch ( AnalysisException $e ) {
			$this->logError( $e );
		}
	}

	public function afterTraverse( array $nodes ) {
		try {
			$this->visitor->afterTraverse( $nodes );
		} catch ( AnalysisException $e ) {
			$this->logError( $e );
		}
	}

	private function logError( AnalysisException $e ) {
		print "WARNING: {$e->getMessage()}\n";
	}
}