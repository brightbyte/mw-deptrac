<?php
namespace Wikimedia\Phanalyst\Visitor;

use PhpParser\Node\Stmt\Trait_;
use Wikimedia\Phanalyst\Analysis\AnalysisException;
use Wikimedia\Phanalyst\Model\Call;
use Wikimedia\Phanalyst\Model\ClassLikeScope;
use Wikimedia\Phanalyst\Model\ClassLikeType;
use Wikimedia\Phanalyst\Model\InstructionScope;
use Wikimedia\Phanalyst\Model\FileScope;
use Wikimedia\Phanalyst\Model\FunctionLikeScope;
use Wikimedia\Phanalyst\Model\FunctionLikeRef;
use Wikimedia\Phanalyst\Model\MethodLikeRef;
use Wikimedia\Phanalyst\Model\MethodLikeScope;
use Wikimedia\Phanalyst\Model\NamespaceScope;
use PhpParser\Node;
use PhpParser\Node\Expr\FuncCall;
use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\NullsafeMethodCall;
use PhpParser\Node\Expr\StaticCall;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassMethod;
use PhpParser\Node\Stmt\Function_;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeVisitorAbstract;
use Wikimedia\Phanalyst\Analysis\CodeBase;

class CallCollector extends NodeVisitorAbstract {
	// TODO: FileLocation
	private string $file;

	private CodeBase $codeBase;
	private ?NamespaceScope $namespace = null;
	private ?ClassLikeScope $class = null;
	private ?FunctionLikeScope $function = null;
	private ?FileScope $fileScope = null;

	/**
	 * @param string $file
	 * @param CodeBase $codeBase
	 */
	public function __construct( string $file, CodeBase $codeBase ) {
		$this->file = $file;
		$this->codeBase = $codeBase;
	}

	private function augmentAttributes( Node $node ) {
		$node->setAttribute( 'filePath', $this->file );
	}

	private function precondition( bool $ok, string $msg, Node $node ) {
		if ( !$ok ) {
			throw new AnalysisException( $msg, $node );
		}
	}

	private function getCurrentNamespace(): NamespaceScope {
		if ( !$this->namespace ) {
			$this->namespace = $this->codeBase->getRootNamespace();
		}

		return $this->namespace;
	}

	private function getCurrentClass( Node $context ): ClassLikeScope {
		$this->precondition( $this->class !== null, 'No current class!', $context );
		return $this->class;
	}

	private function getCurrentLocation(): InstructionScope {
		if ( !$this->function ) {
			if ( !$this->fileScope ) {
				$this->fileScope = new FileScope( $this->file, $this->codeBase->getRootNamespace() );
			}

			return $this->fileScope;
		}

		return $this->function;
	}

	public function enterNode(Node $node) {
		$this->augmentAttributes( $node );

		if ( $node instanceof Function_ ) {
			$this->function = $this->getCurrentNamespace()->getDeclaredFunction( $node->name );
		} elseif ( $node instanceof ClassMethod ) {
			$class = $this->getCurrentClass( $node );
			$this->function = $class->getDeclaredFunction( $node->name );
		} elseif ( $node instanceof Class_ ) {
			$ns = $this->getCurrentNamespace();
			$this->class = $ns->getDeclaredClass( $node->name );
		} elseif ( $node instanceof Trait_ ) {
			$ns = $this->getCurrentNamespace();
			$this->class = $ns->getDeclaredClass( $node->name );
		} elseif ( $node instanceof Namespace_ ) {
			$this->namespace = $this->codeBase->getNamespace( $node->name );
		}
	}

	public function leaveNode(Node $node) {
		if ( $node instanceof Function_ || $node instanceof ClassMethod ) {
			// TODO: keep a stack, for functions declared inside functions
			$this->function = null;
		} elseif ( $node instanceof Class_ ) {
			// TODO: keep a stack, for classes declared inside functions
			$this->class = null;
		} elseif ( $node instanceof Namespace_ ) {
			// TODO: keep a stack, for nested namespaces
			$this->namespace = null;
		} elseif ( $node instanceof New_ || $node instanceof StaticCall ) {
			$this->precondition(
				$this->function !== null,
				'Must be in a function to make call',
				$node
			);

			// FIXME: $node->class may be an expression, need to resolve
			$context = $this->getCurrentClass( $node );
			$type = $this->codeBase->getClassLikeType( $context, $node->class );
			$ref = new MethodLikeRef( $type, $node->name ?? '__construct' );

			$type = $node instanceof New_ ? Call::CONSTRUCTOR_CALL : Call::STATIC_CALL;
			$call = new Call( $this->getCurrentLocation(), $ref, $type );
			$this->codeBase->addCall( $call );
		} elseif ( $node instanceof MethodCall || $node instanceof NullsafeMethodCall ) {
			$function = $this->getCurrentLocation();
			$type = $this->codeBase->resolveExpressionClassLikeType( $function, $node->var );
			$ref = new MethodLikeRef( $type, $node->name );

			$call = new Call( $function, $ref, Call::METHOD_CALL );
			$this->codeBase->addCall( $call );
		} elseif ( $node instanceof FuncCall ) {
			// FIXME: namespace functions?!
			$scope = $this->codeBase->getRootNamespace();

			if ( $node->name instanceof Node\Expr ) {
				throw new AnalysisException( 'Function name expressions are not supported', $node );
			}

			$ref = new FunctionLikeRef( $scope, $node->name );

			$call = new Call( $this->getCurrentLocation(), $ref, Call::FUNCTION_CALL );
			$this->codeBase->addCall( $call );
		}

		// print " -- " . get_class( $node ) . "\n";
	}

}