<?php
namespace Wikimedia\Phanalyst\Analysis;

use PhpParser\Node\ComplexType;
use PhpParser\Node\Expr;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\NullableType;
use PhpParser\Node\Param;
use PhpParser\Node\Stmt\ClassMethod;
use Wikimedia\Phanalyst\Model\Call;
use Wikimedia\Phanalyst\Model\ClassLikeScope;
use Wikimedia\Phanalyst\Model\ClassLikeType;
use Wikimedia\Phanalyst\Model\FunctionDeclarationScope;
use Wikimedia\Phanalyst\Model\FunctionLikeScope;
use Wikimedia\Phanalyst\Model\GlobalScope;
use Wikimedia\Phanalyst\Model\MethodLikeScope;
use Wikimedia\Phanalyst\Model\NamespaceScope;
use Wikimedia\Phanalyst\Model\PrimitiveType;
use Wikimedia\Phanalyst\Model\Scope;
use Wikimedia\Phanalyst\Model\Tag;
use Wikimedia\Phanalyst\Model\Type;

class DebugCodeBase implements CodeBase {

	/**
	 * @var NamespaceScope[]
	 */
	private array $namespaces = [];

	public function closeScopes() {
		foreach ( $this->namespaces as $ns ) {
			$ns->close();
		}
	}

	public function getRootNamespace(): NamespaceScope {
		$qname = '';

		if ( !isset( $this->namespaces[$qname] ) ) {
			$this->namespaces[$qname] = new GlobalScope();
		}

		return $this->namespaces[$qname];
	}

	public function getNamespace( $qname ): NamespaceScope {
		$qname = "$qname";

		if ( !isset( $this->namespaces[$qname] ) ) {
			$this->namespaces[$qname] = new NamespaceScope( $qname );
		}

		return $this->namespaces[$qname];
	}

	/**
	 * @param Name|Identifier|string $qname
	 *
	 * @return array [ Scope, string ]
	 */
	protected function resolveNamespaceForName( Scope $context, $qname ): array {
		if ( $qname instanceof NullableType ) {
			$qname = $qname->type;
		}

		if ( is_string( $qname ) ) {
			$parts = explode( '\\', $qname );
		} elseif ( $qname instanceof Name\FullyQualified ) {
			$parts = $qname->getParts();
		} else {
			$qname = $this->typeToString( $qname );
			$parts = [ $qname ];
		}

		if ( count( $parts ) > 1 ) {
			$relName = array_pop( $parts );

			$nsName = implode( '\\', $parts );
			$ns = $this->getNamespace( $nsName );
			return [ $ns, $relName ];
		} else {
			$ns = $context;
			$name = "$qname";

			// TODO: Inject the root namespace into every namespace, so we can
			// do this in NamespaceScope instead of having a special case here.
			if ( PrimitiveType::isPrimitive( $name ) ) {
				$ns = $this->getRootNamespace();
			}
			return [ $ns, $name ];
		}
	}

	public function getType( Scope $context, $name ): Type {
		/** @var Scope $ns */
		[ $ns, $name ] = $this->resolveNamespaceForName( $context, $name );

		return $ns->getType( $name );
	}

	public function getClassLikeType( Scope $context, $name ): ClassLikeType {
		$node = is_object( $name ) ? $name : null;
		$type = $this->getType( $context, $name );

		if( !$type instanceof ClassLikeType ) {
			throw new AnalysisException(
				'Not a class-like type: ' . $name,
				$node
			);
		}

		return $type;
	}

	/**
	 * @param Name|Identifier|ClassLikeType|string $type
	 *
	 * @return ClassLikeScope
	 */
	public function getDeclaredClass( $qname ): ClassLikeScope {
		// NOTE: $ns can only ever by a NamespaceScope of whatever we pass in
		// as the first param, which in this case is also a NamespaceScope.

		/** @var NamespaceScope $ns */
		[ $ns, $name ] = $this->resolveNamespaceForName( $this->getRootNamespace(), $qname );

		return $ns->getDeclaredClass( $name );
	}

	/**
	 * @param NamespaceScope $namespace
	 * @param Name|Identifier|ClassLikeType|string $name
	 * @param string $type
	 *
	 * @return ClassLikeScope
	 * @throws AnalysisException
	 */
	public function declareClass( NamespaceScope $namespace, $name, string $type ): ClassLikeScope {
		print $namespace . ' contains ' . $type . ' ' . $this->typeToString( $name ) . "\n";
		return $namespace->declareClass( $name, $type );
	}
	/**
	 * @param FunctionDeclarationScope $scope
	 * @param Name|Identifier|ClassLikeType|string $name
	 *
	 * @return FunctionLikeScope
	 */
	public function declareFunction( FunctionDeclarationScope $scope, $name ): FunctionLikeScope {
		print $scope . ' declares function ' . $name . "\n";
		return $scope->declareFunction( $name );
	}

	public function addCall( Call $call ) {
		print $call . "\n";
	}

	public function setFunctionReturnType( FunctionLikeScope $function, $returnType ) {
		$type = $this->getType( $function->getScope(), $returnType );

		// TODO: update $function
		print $function . ' returns ' . $this->typeToString( $type ) . "\n";
	}

	public function setFunctionParameters( FunctionLikeScope $function, array $params ) {
		if ( !$params ) {
			return;
		}

		$paramTypes = $this->getParameterTypeMap( $function->getScope(), $params );

		foreach ( $paramTypes as $name => $type ) {
			$function->declareVarType( $name, $type );
		}

		// TODO: determine parameter types
		// TODO: update $function

		$params = array_map( function ( $p ) {
			return $this->paramToString( $p );
		}, $params );

		print $function . ' takes ' . implode( ',',  $params ) . "\n";
	}

	public function setFunctionModifiers( FunctionLikeScope $function, ClassMethod $node ) {
		// TODO: update $function
		$modifiers = [];

		if ( $node->isAbstract() ) {
			$modifiers[] = 'abstract';
		}

		if ( $node->isFinal() ) {
			$modifiers[] = 'final';
		}

		if ( $node->isMagic() ) {
			$modifiers[] = 'magic';
		}

		if ( $node->isPrivate() ) {
			$modifiers[] = 'private';
		}

		if ( $node->isProtected() ) {
			$modifiers[] = 'protected';
		}

		if ( $node->isPublic() ) {
			$modifiers[] = 'public';
		}

		if ( $node->isStatic() ) {
			$modifiers[] = 'static';
		}

		print $function . ' is ' . implode( ' ', $modifiers ). "\n";
	}

	/**
	 * @param FunctionLikeScope $function
	 * @param Tag[] $tags
	 */
	public function addFunctionTags( FunctionLikeScope $function, array $tags ) {
		// TODO: update $function

		foreach ( $tags as $t ) {
			print $function . ' has ' . $t . "\n";
		}
	}

	public function addClassExtends( ClassLikeScope $class, array $extends ) {
		// TODO: update $class

		foreach ( $extends as $type ) {
			print $class . ' extends ' . $type . "\n";
		}
	}

	public function addClassImplements( ClassLikeScope $class, array $implements ) {
		// TODO: update $class

		foreach ( $implements as $type ) {
			print $class . ' implements ' . $type . "\n";
		}
	}

	public function addClassTraits( ClassLikeScope $class, array $traits ) {
		// TODO: update $class

		foreach ( $traits as $type ) {
			print $class . ' uses ' . $type . "\n";
		}
	}

	public function resolveExpressionType( Scope $scope, Expr $expr ): Type {
		if ( $expr instanceof Expr\Variable ) {
			$type = $scope->getVarType( $expr->name );

			if ( $type === null ) {
				throw new AnalysisException(
					'Name not known in scope: ' . $expr->name,
					$expr
				);
			}

			return $type;
		} else {
			// TODO: handle complex expressions
			throw new AnalysisException(
				'Expression handling not yet implemented for ' . $expr->getType(),
				$expr
			);
		}
	}

	public function resolveExpressionClassLikeType( Scope $scope, Expr $expr ): ClassLikeType {
		$type = $this->resolveExpressionType( $scope, $expr );

		if ( !$type instanceof ClassLikeType ) {
			throw new AnalysisException( 'Not a class-like type: ' . $type, $expr );
		}
		return $type;
	}

	public function resolveExpressionClassLikeScope( Scope $scope, Expr $expr ): ClassLikeScope {
		$type = $this->resolveExpressionClassLikeType( $scope, $expr );
		return $this->getDeclaredClass( $type );
	}

	private function paramToString( Param $p ): string {
		if ( $p->type ) {
			return $this->typeToString( $p->type ) . ' $' . $p->var->name;
		} else {
			return '$' . $p->var->name;
		}
	}

	/**
	 * @param Identifier|Name|ComplexType $type
	 *
	 * @return string
	 */
	private function typeToString( $type ): string {
		if ( $type instanceof NullableType ) {
			return '?' . $type->type;
		}

		// TODO: IntersectionType, UnionType

		return "$type";
	}

	/**
	 * @param Param[] $paramList
	 *
	 * @return Type[]
	 */
	private function getParameterTypeMap( Scope $context, array $paramList ) {
		$types = [];
		foreach ( $paramList as $param ) {
			$type = $this->getType( $context, $param->type ?? 'mixed' ); // FIXME: need to resolve to full QName first!
			$name = $param->var->name;
			$types[$name] = $type;
		}

		return $types;
	}

	/**
	 * @param ClassLikeScope $class
	 * @param Identifier|Name|string $name
	 *
	 * @return MethodLikeScope
	 */
	public function resolveMethodDeclaration( ClassLikeScope $class, $name ): MethodLikeScope {
		// TODO: also check parent class, traits, etc
		$method = $class->getDeclaredFunction( "$name" );

		if ( $method === null ) {
			throw new AnalysisException(
				"Name not known in class {$class->getQName()}: " . $name,
				is_object( $name ) ? $name : null
			);
		}

		return $method;
	}
}