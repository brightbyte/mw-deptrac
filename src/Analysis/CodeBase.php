<?php

namespace Wikimedia\Phanalyst\Analysis;

use PhpParser\Node\ComplexType;
use PhpParser\Node\Expr;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\Param;
use PhpParser\Node\Stmt\ClassMethod;
use Wikimedia\Phanalyst\Model\Call;
use Wikimedia\Phanalyst\Model\ClassLikeScope;
use Wikimedia\Phanalyst\Model\ClassLikeType;
use Wikimedia\Phanalyst\Model\FunctionDeclarationScope;
use Wikimedia\Phanalyst\Model\FunctionLikeScope;
use Wikimedia\Phanalyst\Model\MethodLikeScope;
use Wikimedia\Phanalyst\Model\NamespaceScope;
use Wikimedia\Phanalyst\Model\Scope;
use Wikimedia\Phanalyst\Model\Tag;
use Wikimedia\Phanalyst\Model\Type;

interface CodeBase {

	public function closeScopes();

	public function addCall( Call $call );

	public function getRootNamespace(): NamespaceScope;

	/**
	 * @param Name|string $name
	 *
	 * @return NamespaceScope
	 */
	public function getNamespace( $name ): NamespaceScope;

	/**
	 * @param Scope $context
	 * @param Name|Identifier|string $name
	 *
	 * @return ClassLikeType
	 */
	public function getClassLikeType( Scope $context, $name ): ClassLikeType;

	/**
	 * @param Scope $context
	 * @param Name|Identifier|string $name
	 *
	 * @return ClassLikeType
	 */
	public function getType( Scope $context, $name ): Type;

	/**
	 * @param Name|Identifier|ClassLikeType|string $type
	 *
	 * @return ClassLikeScope
	 */
	public function getDeclaredClass( $type ): ClassLikeScope;

	/**
	 * @param NamespaceScope $namespace
	 * @param Name|Identifier|ClassLikeType|string $name
	 * @param string $type See ClassLikeType::XXX_TYPE
	 *
	 * @return ClassLikeScope
	 */
	public function declareClass( NamespaceScope $namespace, $name, string $type ): ClassLikeScope;

	/**
	 * @param FunctionDeclarationScope $scope
	 * @param Name|Identifier|ClassLikeType|string $name
	 *
	 * @return FunctionLikeScope
	 */
	public function declareFunction( FunctionDeclarationScope $scope, $name ): FunctionLikeScope;

	/**
	 * @param FunctionLikeScope $function
	 * @param Identifier|Name|ComplexType|Type|string $returnType
	 */
	public function setFunctionReturnType(
		FunctionLikeScope $function,
		$returnType
	);

	/**
	 * @param FunctionLikeScope $function
	 * @param Param[] $getParams
	 */
	public function setFunctionParameters(
		FunctionLikeScope $function,
		array $params
	);

	/**
	 * @param FunctionLikeScope $function
	 * @param ClassMethod $node
	 */
	public function setFunctionModifiers(
		FunctionLikeScope $function,
		ClassMethod $node
	);

	/**
	 * @param FunctionLikeScope $function
	 * @param Tag[] $tags
	 */
	public function addFunctionTags(
		FunctionLikeScope $function,
		array $tags
	);

	/**
	 * @param ClassLikeScope $class
	 * @param ClassLikeType[]|Name[] $extends
	 */
	public function addClassExtends(
		ClassLikeScope $class,
		array $extends
	);

	/**
	 * @param ClassLikeScope $class
	 * @param ClassLikeType[]|Name[] $implements
	 */
	public function addClassImplements(
		ClassLikeScope $class,
		array $implements
	);

	/**
	 * @param ClassLikeScope $class
	 * @param ClassLikeType[]|Name[] $traits
	 */
	public function addClassTraits(
		ClassLikeScope $class,
		array $traits
	);

	public function resolveExpressionClassLikeType( Scope $scope, Expr $expr ) : ClassLikeType;

	public function resolveExpressionClassLikeScope( Scope $scope, Expr $expr ) : ClassLikeScope;

	public function resolveExpressionType( Scope $scope, Expr $expr ) : Type;

	/**
	 * @param ClassLikeScope $class
	 * @param Identifier|Name|string $name
	 *
	 * @return MethodLikeScope
	 */
	public function resolveMethodDeclaration( ClassLikeScope $class, $name ): MethodLikeScope;

}