<?php

namespace Wikimedia\Phanalyst\Analysis;

use PhpParser\Node;

class AnalysisException extends \Exception {

	private ?Node $node;

	public function __construct( string $msg, ?Node $node = null ) {
		if ( $node ) {
			$location = self::getLocationString( $node );

			if ( $location ) {
				$msg = "$location: $msg";
			}
		}

		// TODO: add file and line to msg
		parent::__construct( $msg );
		$this->node = $node;
	}

	private static function getLocationString( Node $node ) {
		$line = $node->getAttribute( 'startLine' );
		$file = $node->getAttribute( 'filePath' );

		if ( $line && $file ) {
			return "$file($line)";
		} elseif ( $line ) {
			return "line $line";
		} elseif ( $line ) {
			return "$file";
		} else {
			return '';
		}
	}

	public function getNode(): ?Node {
		return $this->node;
	}

	public function getNodeAttributes(): array {
		return $this->node ? $this->node->getAttributes() : [];
	}

}