<?php
namespace Wikimedia\Phanalyst\Model;

class MethodLikeRef extends CallTargetRef {

	private ClassLikeType $type;

	public function __construct( ClassLikeType $type, string $name ) {
		$this->type = $type;
		parent::__construct( $name );
	}

	public function getQName() {
		return $this->type->qualify( $this );
	}

}