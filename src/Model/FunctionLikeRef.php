<?php
namespace Wikimedia\Phanalyst\Model;

class FunctionLikeRef extends CallTargetRef {

	private Scope $scope;

	// TODO: modifiers/tags
	public function __construct( Scope $scope, string $name ) {
		parent::__construct( $name );
		$this->scope = $scope;
	}

	public function getQName() {
		return $this->scope->qualify( $this );
	}

}