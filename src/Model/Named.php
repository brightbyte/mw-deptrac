<?php
namespace Wikimedia\Phanalyst\Model;

interface Named {

	public function getName();

	public function __toString();

}