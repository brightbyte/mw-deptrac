<?php
namespace Wikimedia\Phanalyst\Model;

use Wikimedia\Phanalyst\Analysis\AnalysisException;

class FunctionLikeScope extends AbstractScope implements InstructionScope {

	private Scope $scope;

	/**
	 * @param Scope $scope
	 * @param string $name
	 */
	public function __construct( Scope $scope, string $name ) {
		parent::__construct( $name );
		$this->scope = $scope;
	}

	/**
	 * @return Scope
	 */
	public function getScope() : Scope {
		return $this->scope;
	}

	public function getQName() : string {
		return $this->scope->qualify( $this );
	}

	public function getSuffix() : string {
		return '##'; // HACK!
	}

	public function getType( $name ): Type {
		return $this->scope->getType( $name );
	}

}