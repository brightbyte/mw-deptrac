<?php
namespace Wikimedia\Phanalyst\Model;

use PhpParser\Node\ComplexType;
use PhpParser\Node\Identifier;
use PhpParser\Node\Name;
use PhpParser\Node\NullableType;
use Wikimedia\Phanalyst\Analysis\AnalysisException;

class NamespaceScope extends FunctionDeclarationScope {

	private $types = [];
	private $classScopes = [];

	/**
	 * @param string $name
	 */
	public function __construct( string $name ) {
		parent::__construct( $name );
	}

	public function getQName() : string {
		return $this->getName();
	}

	protected function getSuffix() : string {
		return '\\';
	}

	protected function addType( Type $type ) {
		$name = $type->getName();

		if ( !preg_match( '/^\w+$/', $name ) ) {
			throw new AnalysisException( 'Bad type name: ' . $name );
		}

		if ( isset( $this->types[$name] ) ) {
			throw new AnalysisException( 'Type already defined: ' . $name );
		}

		$this->types[$name] = $type;
	}

	public function getType( $name ): Type {
		$node = is_object( $name ) ? $name : null;
		$name = $this->typeToString( $name );

		if ( !isset( $this->types[$name] ) ) {
			if ( !$this->isOpen ) {
				throw new AnalysisException( 'Unknown type: ' . $name, $node );
			}

			$this->addType( new ClassLikeType( $this, $name ) );
		}

		return $this->types[$name];
	}

	/**
	 * @param Name|Identifier|ClassLikeType|string $type
	 *
	 * @return ClassLikeScope
	 */
	public function getDeclaredClass( $type ): ClassLikeScope {
		$node = is_object( $type ) ? $type : null;
		$name = $this->typeToString( $type );

		if ( !isset( $this->classScopes[$name] ) ) {
			throw new AnalysisException( 'Unknown class: ' . $name, $node );
		}
		return $this->classScopes[$name];
	}

	/**
	 * @param Name|Identifier|ClassLikeType|string $name
	 * @param string $type See ClassLikeType::XXX_TYPE
	 *
	 * @return ClassLikeScope
	 */
	public function declareClass( $name, string $type ): ClassLikeScope {
		$node = is_object( $name ) ? $name : null;
		$name = $this->typeToString( $name );

		if ( isset( $this->classScopes[$name] ) ) {
			throw new AnalysisException( 'Class already defined: ' . $name, $node );
		}

		$scope = new ClassLikeScope( $this, $name, $type );
		$this->classScopes[$name] = $scope;

		$this->addType( $scope->getClassType() );
		return $scope;
	}

	/**
	 * @param Identifier|Name|ComplexType $type
	 *
	 * @return string
	 */
	private function typeToString( $type ): string {
		if ( $type instanceof NullableType ) {
			$type = $type->type;
		}
		if ( $type instanceof Type ) {
			$type = $type->getName();
		}

		return "$type";
	}

}