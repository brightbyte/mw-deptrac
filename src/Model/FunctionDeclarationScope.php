<?php
namespace Wikimedia\Phanalyst\Model;

use Wikimedia\Phanalyst\Analysis\AnalysisException;

abstract class FunctionDeclarationScope extends AbstractScope {

	private array $functions = [];

	/**
	 * @param string $name
	 */
	public function __construct( string $name ) {
		parent::__construct( $name );
	}

	public function declareFunction( string $name ): FunctionLikeScope {
		$func = new FunctionLikeScope( $this, $name );
		$this->addDeclaredFunction( $func );
		return $func;
	}

	public function addDeclaredFunction( FunctionLikeScope $function ) {
		$name = $function->getName();

		if ( isset( $this->functions[$name] ) ) {
			throw new AnalysisException( "Duplicate function in {$this->getQName()}: $name" );
		}

		$this->functions[$name] = $function;
	}

	public function getDeclaredFunction( string $name ): ?FunctionLikeScope {
		return $this->functions[$name] ?? null;
	}

}