<?php

namespace Wikimedia\Phanalyst\Model;

class PrimitiveType extends Type {
	public const TYPES = [
		'string',
		'int',
		'float',
		'bool',
		'callable',
		'null',
		'array',
		'object'
	];

	public static function isPrimitive( string $type ) {
		return in_array( $type, self::TYPES );
	}
}