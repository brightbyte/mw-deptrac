<?php

namespace Wikimedia\Phanalyst\Model;

class ClassLikeType extends Type implements QNamed {
	public const CLASS_TYPE = 'class';
	public const INTERFACE_TYPE = 'interface';
	public const TRAIT_TYPE = 'trait';

	private string $namespace;

	/**
	 * @param string $name
	 */
	public function __construct( string $namespace, string $name ) {
		parent::__construct( $name );
		$this->namespace = $namespace;
	}

	/**
	 * @return string
	 */
	public function getNamespace() : string {
		return $this->namespace;
	}

	public function getQName() {
		return $this->namespace . '\\' . $this->getName();
	}

	public function __toString() {
		return $this->getQName();
	}

	public function qualify( Named $named ) {
		return $this->getQName() . '::' . $named->getName();
	}
}