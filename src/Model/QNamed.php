<?php
namespace Wikimedia\Phanalyst\Model;

interface QNamed extends Named {

	public function getQName();

}