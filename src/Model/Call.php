<?php
namespace Wikimedia\Phanalyst\Model;

class Call {
	public const STATIC_CALL = 'static';
	public const CONSTRUCTOR_CALL = 'constructor';
	public const FUNCTION_CALL = 'function';
	public const METHOD_CALL = 'method';

	private InstructionScope $location;
	private CallTargetRef $target;
	private string $type;

	/**
	 * @param InstructionScope $location
	 * @param CallTargetRef $target
	 * @param array $modifiers
	 */
	public function __construct( InstructionScope $location, CallTargetRef $target, string $type ) {
		$this->location = $location;
		$this->target = $target;

		$this->type = $type;
	}

	public function __toString() {
		return $this->location . ' calls ' . $this->type . ' ' . $this->target;
	}

}