<?php
namespace Wikimedia\Phanalyst\Model;

interface Scope extends QNamed {

	public function close();

	public function declareVarType( string $name, Type $type );

	public function getVarType( string $name ): ?Type;

	public function getType( string $name ): Type;

	public function qualify( Named $named ): string;

}