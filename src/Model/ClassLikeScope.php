<?php
namespace Wikimedia\Phanalyst\Model;

use Wikimedia\Phanalyst\Analysis\AnalysisException;

class ClassLikeScope extends FunctionDeclarationScope {

	private NamespaceScope $namespace;
	private ClassLikeType $classType;
	private string $type;

	/**
	 * @param NamespaceScope $namespace
	 * @param string $name
	 * @param string $type See ClassLikeType::XXX_TYPE
	 */
	public function __construct( NamespaceScope $namespace, string $name, string $type ) {
		parent::__construct( $name );
		$this->namespace = $namespace;

		// FIXME: doesn't work for Traits! Collect all users of the trait into a union type?
		$this->classType = new ClassLikeType( $namespace->getQName(), $name );
		$this->type = $type;
	}

	/**
	 * @return string One of the ClassLikeType::XXX_TYPE conbstants
	 */
	public function getScopeType(): string {
		return $this->type;
	}

	public function getVarType( string $name ): ?Type {
		if ( $name === 'this' ) {
			// FIXME: how to handle this in traits?!
			return $this->getClassType();
		}
		return parent::getVarType( $name );
	}

	public function getDeclaredFunction( string $name ): ?MethodLikeScope {
		/** @var MethodLikeScope $func */
		$func = parent::getDeclaredFunction( $name );
		return $func;
	}

	public function declareFunction( string $name ): MethodLikeScope {
		$func = new MethodLikeScope( $this, $name );
		$this->addDeclaredFunction( $func );
		return $func;
	}

	public function addDeclaredFunction( FunctionLikeScope $function ) {
		if ( !$function instanceof MethodLikeScope ) {
			throw new AnalysisException(
				'Unexpected function declaration in class scope'
			);
		}
		parent::addDeclaredFunction( $function );
	}

	public function getClassType(): ClassLikeType {
		return $this->classType;
	}

	public function getParentType(): ClassLikeType {
		if ( $this->type !== ClassLikeType::CLASS_TYPE ) {
			throw new AnalysisException(
				"Only classes have parent types, {$this->getQName()} is a {$this->type}!"
			);
		}

		// TODO: get this from the list of types this class extends
		throw new AnalysisException(
			'Parent class access not yet implemented!'
		);
	}

	public function getType( $name ): Type {
		if ( $name === 'static' || $name === 'self' ) {
			return $this->getClassType();
		} elseif ( $name === 'parent' ) {
			return $this->getParentType();
		}

		return $this->namespace->getType( $name );
	}

	public function getQName() : string {
		return $this->namespace->qualify( $this );
	}

	public function getSuffix() : string {
		return '::';
	}

}