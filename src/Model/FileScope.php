<?php
namespace Wikimedia\Phanalyst\Model;

class FileScope extends AbstractScope implements InstructionScope {

	private Scope $globalScope;

	private string $filePath;

	/**
	 * @param NamespaceScope $namespace
	 * @param string $name
	 */
	public function __construct( string $filePath, Scope $globalScope ) {
		parent::__construct( $filePath );
		$this->filePath = $filePath;
		$this->globalScope = $globalScope;
	}

	public function getFilePath() : string {
		return $this->filePath;
	}

	public function getVarType( string $name ): ?Type {
		return $this->globalScope->getVarType( $name );
	}

	public function declareVarType( string $name, Type $type ) {
		$this->globalScope->declareVarType( $name, $type );
	}

	public function qualify( Named $named ) : string {
		return $this->globalScope->qualify( $named );
	}

	public function getQName(): string {
		return $this->getFilePath();
	}

	protected function getSuffix(): string {
		return '##';
	}

	public function getType( string $name ) : Type {
		return $this->globalScope->getType( $name );
	}
}