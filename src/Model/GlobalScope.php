<?php
namespace Wikimedia\Phanalyst\Model;

class GlobalScope extends NamespaceScope {

	public function __construct() {
		parent::__construct( '' );

		$this->declarePrimitiveType( 'string' );
		$this->declarePrimitiveType( 'int' );
		$this->declarePrimitiveType( 'float' );
		$this->declarePrimitiveType( 'bool' );
		$this->declarePrimitiveType( 'callable' );
		$this->declarePrimitiveType( 'null' );
	}

	private function declarePrimitiveType( string $name ) {
		$this->addType( new PrimitiveType( $name ) );
	}

	public function getQName(): string {
		return '';
	}

	protected function getSuffix(): string {
		return '\\';
	}
}