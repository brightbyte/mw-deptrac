<?php
namespace Wikimedia\Phanalyst\Model;

class Variable implements Named {
	private Scope $scope;
	private string $name;
	private Type $type;

	/**
	 * @param Scope $scope
	 * @param string $name
	 */
	public function __construct( Scope $scope, string $name, Type $type ) {
		$this->scope = $scope;
		$this->name = $name;
		$this->type = $type;
	}

	/**
	 * @return Type
	 */
	public function getType() : Type {
		return $this->type;
	}

	/**
	 * @return string
	 */
	public function getName() : string {
		return $this->name;
	}

	public function getQName() : string {
		return $this->scope->qualify( $this );
	}

	public function __toString() {
		return $this->getQName();
	}
}