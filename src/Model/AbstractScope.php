<?php
namespace Wikimedia\Phanalyst\Model;

use Wikimedia\Phanalyst\Analysis\AnalysisException;

abstract class AbstractScope implements Scope {

	private string $name;

	protected bool $isOpen = true;

	/**
	 * @var Type[] Map of name to type
	 */
	private array $vars = [];

	/**
	 * @param string $name
	 */
	public function __construct( string $name ) {
		$this->name = $name;
	}

	public function __toString() {
		return $this->getQName();
	}

	public function close() {
		$this->isOpen = false;
	}

	public abstract function getQName(): string;

	public function declareVarType( string $name, Type $type ) {
		if ( !preg_match( '/^\w+$/', $name ) ) {
			throw new AnalysisException( 'Bad var name: ' . $name );
		}

		$this->vars[$name] = $type;
	}

	public function getVarType( string $name ): ?Type {
		return $this->vars[$name] ?? null;
	}

	public abstract function getType( string $name ): Type;

	public function getName() : string {
		return $this->name;
	}

	protected abstract function getSuffix(): string;

	public function qualify( Named $named ): string {
		return $this->getQName() . $this->getSuffix() . $named->getName();
	}

}