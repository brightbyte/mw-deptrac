<?php
namespace Wikimedia\Phanalyst\Model;

/**
 * A scope that can contain instructions (rather than just declarations).
 */
interface InstructionScope extends Scope {

}