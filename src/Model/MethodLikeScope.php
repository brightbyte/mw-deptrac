<?php
namespace Wikimedia\Phanalyst\Model;

use PhpParser\Node\Expr;

class MethodLikeScope extends FunctionLikeScope {

	private ClassLikeScope $classScope;

	/**
	 * @param ClassLikeScope $scope
	 * @param string $name
	 */
	public function __construct( ClassLikeScope $scope, string $name ) {
		// TODO: modifiers/tags
		parent::__construct( $scope, $name );
		$this->classScope = $scope;
	}

	public function getVarType( string $name ): ?Type {
		if ( $name === 'this' ) {
			// FIXME: how to handle this in traits?!
			return $this->classScope->getClassType();
		}
		return parent::getVarType( $name );
	}

}