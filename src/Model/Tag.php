<?php

namespace Wikimedia\Phanalyst\Model;

class Tag implements Named {

	private string $name;
	private string $value;

	/**
	 * @param string $name
	 */
	public function __construct( string $name, string $value ) {
		$this->name = $name;
		$this->value = $value;
	}

	public function getValue() : string {
		return $this->value;
	}

	public function getName() {
		return $this->name;
	}

	public function __toString() {
		return '@' . $this->name . ' ' . $this->value;
	}
}