<?php

namespace Wikimedia\Phanalyst;

use ArrayIterator;
use CallbackFilterIterator;
use Exception;
use Iterator;
use IteratorIterator;
use PhpParser\Node;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\Parser;
use PhpParser\ParserFactory;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RecursiveRegexIterator;
use Wikimedia\Phanalyst\Analysis\CodeBase;
use Wikimedia\Phanalyst\Analysis\DebugCodeBase;
use Wikimedia\Phanalyst\Visitor\CallCollector;
use Wikimedia\Phanalyst\Visitor\DeclarationCollector;
use Wikimedia\Phanalyst\Visitor\LenientCollector;

class Phanalyzer {

	/**
	 * @var string[]
	 */
	private array $files;
	private CodeBase $codeBase;
	private Parser $parser;
	private array $astCache = [];

	public function __construct( array $files ) {

		// TODO: traverse directories, etc
		$this->files = $files;
		$this->codeBase = new DebugCodeBase();
		$this->parser = ( new ParserFactory() )->createForNewestSupportedVersion();
	}

	private function getFileIterator(): Iterator {
		$iterators = [];

		foreach ( $this->files as $root ) {
			if ( is_dir( $root ) ) {
				$dir = new RecursiveIteratorIterator(
					new RecursiveDirectoryIterator( $root )
				);

				$iter = new CallbackFilterIterator(
					$dir,
					static function ( $name ) {
						return preg_match( '/\.php$/i', $name );
					}
				);
			} else {
				$iter = new ArrayIterator( [ $root ] );
			}


			$iterators[] = $iter;
		}

		if ( count( $iterators ) !== 1 ) {
			return new IteratorIterator( new ArrayIterator( $iterators ) );
		} else {
			return reset( $iterators );
		}
	}

	public function main() {
		$files = $this->getFileIterator();

		$this->collectDeclarations( $files );
		$this->codeBase->closeScopes();

		$files = $this->getFileIterator();
		$this->collectCalls( $files );
	}

	private function getAST( string $file ) {
		//  TODO: make caching optional for large code bases

		if ( !isset( $this->astCache[$file] ) ) {
			$this->astCache[$file] = $this->parse( $file );
		}

		return $this->astCache[$file];
	}

	/**
	 * @param string $file
	 *
	 * @return Node[]
	 */
	private function parse( string $file ): array {
		$code = file_get_contents( $file );
		$parser = $this->getParser();
		$ast = $parser->parse($code);

		$traverser = new NodeTraverser();
		$traverser->addVisitor( new NameResolver() );

		$ast = $traverser->traverse( $ast );
		return $ast;
	}

	/**
	 * @param string[] $files
	 */
	public function collectDeclarations( iterable $files ): void {
		foreach ( $files as $f ) {
			$this->collectDeclarationsFromFile( $f );
		}
	}

	public function collectCalls( iterable $files ) {
		foreach ( $files as $f ) {
			$this->collectCallsFromFile( $f );
		}
	}

	public function collectDeclarationsFromFile( string $file ): void {
		$ast = $this->getAST( $file );

		$traverser = new NodeTraverser();
		$traverser->addVisitor(
			new LenientCollector(
				new DeclarationCollector( $file, $this->codeBase )
			)
		);
		$traverser->traverse( $ast );
	}

	public function collectCallsFromFile( string $file ): void {
		$ast = $this->getAST( $file );

		$traverser = new NodeTraverser();
		$traverser->addVisitor(
			new LenientCollector(
				new CallCollector( $file, $this->codeBase )
			)
		);
		$traverser->traverse( $ast );
	}

	/**
	 * @return Parser
	 */
	private function getParser() : Parser {
		return $this->parser;
	}

}

include __DIR__ . '/../vendor/autoload.php';

if ( !isset( $argv[1] ) ) {
	echo "Missing file name\n";
	die(1);
}

$analyzer = new Phanalyzer( array_slice( $argv, 1 ) );

try {
	$analyzer->main();
} catch (Exception $error) {
	echo "Parse error: {$error->getMessage()}\n";
	exit(2);
}