<?php

namespace Wikimedia\Phanalyzer\Test {
	include __DIR__ . '/../../vendor/autoload.php';

	use Wikimedia\Phanalyst\Model\PrimitiveType;

	$x = new PrimitiveType( 'xx' );
}

namespace Wikimedia\Phanalyzer\Test2 {

	use Wikimedia\Phanalyst\Analysis\DebugCodeBase;

	$dbg = new DebugCodeBase();

	print get_class( $x ) . "\n";
}

